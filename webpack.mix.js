const mix = require('laravel-mix');
const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const { VueLoaderPlugin } = require('vue-loader')
const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin')
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
// Main const
const PATHS = {
    src: path.join(__dirname, '/resources'),
    dist: path.join(__dirname, '/public'),
    assets: 'assets/'
}

// mix.js(PATHS.src + '/js/app.js', PATHS.dist + '/js')
//    .sass(PATHS.src + '/sass/main.scss', PATHS.dist + '/css');

module.exports = {
    // BASE config
    externals: {
        paths: PATHS
    },
    entry: {
        app: PATHS.src,
    },
    output: {
        filename: `${PATHS.assets}js/[name].[hash].js`,
        path: PATHS.dist,
        publicPath: '/'
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {
                    name: 'vendors',
                    test: /node_modules/,
                    chunks: 'all',
                    enforce: true
                }
            }
        }
    },
    module: {
        rules: [{
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: '/node_modules/'
        }, {
            test: /\.vue$/,
            loader: 'vue-loader',
            options: {
                loader: {
                    scss: 'vue-style-loader!css-loader!sass-loader'
                }
            }
        }, {
            test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'file-loader',
            options: {
                name: `${PATHS.assets}fonts/[name].[ext]`
            }
        }, {
            test: /\.(png|jpe?g|gif|svg|ico)$/,
            loader: 'file-loader',
            options: {
                name: '[name].[ext]'
            }
        }, {
            test: /\.scss$/,
            use: [
                'style-loader',
                MiniCssExtractPlugin.loader,
                {
                    loader: 'css-loader',
                    options: { sourceMap: true }
                }, {
                    loader: 'postcss-loader',
                    options: { sourceMap: true, config: { path: `./postcss.config.js` } }
                }, {
                    loader: 'sass-loader',
                    options: { sourceMap: true }
                }
            ]
        }, {
            test: /\.css$/,
            use: [
                'style-loader',
                MiniCssExtractPlugin.loader,
                {
                    loader: 'css-loader',
                    options: { sourceMap: true }
                }, {
                    loader: 'postcss-loader',
                    options: { sourceMap: true, config: { path: `./postcss.config.js` } }
                }
            ]
        }
        ]
    },
    resolve: {
        alias: {
            '~': PATHS.src,
            'vue$': 'vue/dist/vue.min.js',
        }
    },
    plugins: [
        new VueLoaderPlugin(),
        new MiniCssExtractPlugin({
            filename: `${PATHS.assets}css/[name].[hash].css`,
        }),
        new CopyWebpackPlugin([
            { from: `${PATHS.src}/${PATHS.assets}img`, to: `${PATHS.dist}/${PATHS.assets}img` },
            //{ from: `${PATHS.src}/${PATHS.assets}fonts`, to: `${PATHS.assets}fonts` },
            { from: `${PATHS.src}/static`, to: '' },
        ]),
        // new SVGSpritemapPlugin(`${PATHS.src}/${PATHS.assets}img/icons-svg/**/*.svg`, {
        //     output: {
        //         svg: {
        //             // Disable `width` and `height` attributes on the root SVG element
        //             // as these will skew the sprites when using the <view> via fragment identifiers
        //             sizes: false
        //         },
        //         svg4everybody: true,
        //         filename: 'assets/img/spriteSvg.svg'
        //     },
        //     sprite: {
        //         generate: {
        //             // Generate <use> tags within the spritemap as the <view> tag will use this
        //             use: true,
        //
        //             // Generate <view> tags within the svg to use in css via fragment identifier url
        //             // and add -fragment suffix for the identifier to prevent naming colissions with the symbol identifier
        //             view: '-fragment',
        //
        //             // Generate <symbol> tags within the SVG to use in HTML via <use> tag
        //             symbol: true
        //         },
        //     },
        //     styles: {
        //         // Specifiy that we want to use URLs with fragment identifiers in a styles file as well
        //         format: 'fragment',
        //
        //         // Path to the styles file, note that this method uses the `output.publicPath` webpack option
        //         // to generate the path/URL to the spritemap itself so you might have to look into that
        //         //filename: path.join(__dirname, 'src/scss/_sprites.scss')
        //     }
        // }),
    ],
}
