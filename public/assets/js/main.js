jQuery(".remove-from-cart").click(function (e) {
	console.log('Delete');
	e.preventDefault();
	var ele = jQuery(this);

	if(confirm("Вы уверены?")) {
		jQuery.ajax({
			url: '/order/removefromcart',
			method: "DELETE",
			data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
			success: function (response) {
				window.location.reload();
			}
		});
	}
}); 