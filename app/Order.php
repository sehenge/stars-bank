<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['user_id', 'star_id', 'name', 'address', 'advert_text', 'price', 'status', 'additional_options'];
}
