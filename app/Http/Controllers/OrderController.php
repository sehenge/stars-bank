<?php

namespace App\Http\Controllers;

use App\Order;
use App\Star;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(int $id)
    {
        $star = Star::find($id);
        
        if(!$star) { 
            abort(404); 
        }

        return view('cabinet.order', ['star' => $star]);
    }

    public function place(Request $request)
    {
//        $this->validator($request->all())->validate();
    //    dd($request->all());

        $session = session()->get('cart');
        $user = auth()->user();

        try {
            foreach ($session as $data) {
                $result = Order::create([
                    'user_id' => $user->id,
                    'star_id' => $data['star_id'],
                    'name' => $data['adv_name'],
                    'address' => $data['adv_address'],
                    'advert_text' => $data['adv_text'],
                    'price' => $data['price'],
                    'status' => 0,
                    'additional_options' => $data['additional_options'],
                ]);

            }

            session()->forget('cart');
            
            return redirect()->route('cabinet.history');
        } catch (\Exception $exception) {
            return redirect()->route('order')
                ->withErrors(['message' => json_encode($exception)]);
        }
    }

    public function showHistory()
    {
        $orders = Order::all();
        
        foreach ($orders as $order) {
            if ($order->status === 0) $order->status = 'Не оплачен';
            if ($order->status === 1) $order->status = 'В процессе';
            if ($order->status === 2) $order->status = 'Выполнен';
        }

        return view('cabinet.history', ['orders' => $orders]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        $star = Star::find($id);

        if ($order->status === 0) $order->status = 'Не оплачен';
        if ($order->status === 1) $order->status = 'В процессе';
        if ($order->status === 2) $order->status = 'Выполнен';

        $order->additional_options = explode(',', $order->additional_options);

        return view('cabinet.order-info', ['order' => $order]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
