<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::user();

        return view('cabinet.profile', ['user' => $user]);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function verificationPage()
    {
        $user = Auth::user();

        return view('cabinet.verification', ['user' => $user]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $user = Auth::user();
        try {
            User::whereId($user->id)->update($request->except('_token'));

            return redirect()->back()->withSuccess('Изменения успешно сохранены');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['message' => 'Не удалось сохранить изменения']);
        }

    }

}
