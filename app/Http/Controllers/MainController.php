<?php

namespace App\Http\Controllers;

use App\Star;

class MainController extends Controller
{
    public function index()
    {
        $stars = Star::all();

        return view('main', ['stars' => $stars]);
    }
}
