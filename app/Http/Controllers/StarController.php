<?php

namespace App\Http\Controllers;

use App\Star;
use Illuminate\Http\Request;

class StarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function addToCart(Request $request)
    {
        $star_id = $request->star_id;        
        $star = Star::find($star_id);

        if(!$star) { 
            abort(404); 
        }
 
        $cart = session()->get('cart');
        
        $cart[] = [
            "star_id" => $star_id,
            "star_name" => $star->name,
            "star_description" => $star->description,
            "quantity" => 1,
            "price" => $star->price,
            "duration" => $star->duration,
            "image" => $star->image,
            "additional_options" => implode(",", $request->additional_options),

            "adv_address" => $request->address,
            "adv_text" => $request->text,
            "adv_name" => $request->name,
        ];
 
        session()->put('cart', $cart); 
        
        return redirect()->route('cabinet.cart')->with('success', 'Продукт успешно добавлен в корзину!');
    }

    public function removeFromCart(Request $request)
    {
        if(isset($request->id)) { 
            $cart = session()->get('cart'); 
            if(isset($cart[$request->id])) { 
                unset($cart[$request->id]); 
                session()->put('cart', $cart);
            }
 
            session()->flash('success', 'Продукт удален из корзины');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Star  $star
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $star = Star::find($id);
        
        if(!$star) { 
            abort(404); 
        }

        return view('product-info', ['star' => $star]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Star  $star
     * @return \Illuminate\Http\Response
     */
    public function edit(Star $star)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Star  $star
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Star $star)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Star  $star
     * @return \Illuminate\Http\Response
     */
    public function destroy(Star $star)
    {
        //
    }
}
