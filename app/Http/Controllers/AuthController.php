<?php

namespace App\Http\Controllers;

use App\Mail\MailtrapExample;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Nexmo\SMS\Message\SMS;


class AuthController extends Controller
{
    use AuthenticatesUsers, RegistersUsers {
        AuthenticatesUsers::redirectPath insteadof RegistersUsers;
        AuthenticatesUsers::guard insteadof RegistersUsers;
    }

    protected $redirectTo = '/';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|Response|\Illuminate\View\View

     */
    public function index()
    {
        return view('auth.login');
    }

    /**
     * Create a new controller instance.
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $input = $request->all();

        $remember = isset($input['remember']) ? true : false;
        $validator = $this->validate($request, [ //TODO: add new validation for ti_number
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $fieldType = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'ti_number';
        if (auth()->attempt(array($fieldType => $input['email'], 'password' => $input['password']), $remember)) {
            return redirect()->route('index');
        } else {
            return redirect()->route('login')
                ->withErrors(['message' => __('Неверно указаны логин/ИНН или пароль')]); //todo: change message and add correct validation
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'address' => 'required|max:255',
            'name' => 'required|max:255',
            'ogrn' => 'required|min:13|max:13',
            'password' => 'required|min:1|confirmed',
        ]);
    }

    /**
     *
     * @param array $data
     * @return RedirectResponse
     */
    public function create(array $data)
    {
        try {
            $result = User::create([
                'name' => $data['name'],
                'role_id' => 1,
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'legal_address' => $data['address'],
                'ogrn' => $data['ogrn'],
                'ti_number' => $data['ti_number'],
                'fio' => $data['fio'],
                'phone' => $data['phone'],
            ]);

            return $result;
        } catch (\Exception $exception) {
            dd($exception);
            return redirect()->route('index')
                ->withErrors(['message' => $exception->errorInfo[2]]); //todo: change message and add correct validation
        }
    }


    public function phoneVerificationSendCode(Request $request) //todo: add return with redirect
    {
        $basic = new \Nexmo\Client\Credentials\Basic(getenv('NEXMO_KEY'), getenv('NEXMO_SECRET'));
        $client = new \Nexmo\Client($basic);

        $verification = $client->verify()->start([
            'number' => $request->nexmomobile,
            'brand' => 'Vonage',
            'code_length' => '4']);

        $user = Auth::user();
        $user->nexmo_request_id = $verification->getRequestId();
        $user->phone = $request->nexmomobile;
        $user->save();
    }

    public function phoneVerify(Request $request) //TODO: add return with redirect and check exception Nexmo\Client\Exception\Request
        //The Nexmo platform was unable to process this message for the following reason: Request '2c90de0b8ca447ad8445546a6ef5a7fa' was not found or it has been verified already.
    {
        $basic = new \Nexmo\Client\Credentials\Basic(getenv('NEXMO_KEY'), getenv('NEXMO_SECRET'));
        $client = new \Nexmo\Client($basic);

        $user = Auth::user();
        $request_id = $user->nexmo_request_id;

        $verification = new \Nexmo\Verify\Verification($request_id);
        $result = $client->verify()->check($verification, $request->nexmocode);
        
        if ($result) {
            $user->phone_verification_code = $request->nexmocode;
            $user->phone_verified_at = now();
            $user->save();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Auth $auth
     * @return Response
     */
    public function show(Auth $auth)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Auth $auth
     * @return Response
     */
    public function edit(Auth $auth)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Auth $auth
     * @return Response
     */
    public function update(Request $request, Auth $auth)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Auth $auth
     * @return Response
     */
    public function destroy(Auth $auth)
    {
        //
    }
}
