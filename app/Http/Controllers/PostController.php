<?php

namespace App\Http\Controllers;

use TCG\Voyager\Models\Category;
use TCG\Voyager\Models\Post;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::all();

        return view('posts', ['posts' => $posts]);
    }
}
