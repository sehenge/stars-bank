<?php

namespace App\Http\Middleware;

use Closure;

class CheckPhone
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user()) {
            return redirect('login');
        }

        if (!$request->user()->phone_verified_at) {
            return redirect('verification');
        }

        return $next($request);
    }
}
