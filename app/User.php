<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use TCG\Voyager\Models\User as VUser;

class User extends VUser implements MustVerifyEmail
{
    use Notifiable;

    protected $fillable = ['name', 'email', 'password', 'legal_address', 'ogrn', 'ti_number', 'fio', 'phone'];
    protected $guarded = ['_token'];
}