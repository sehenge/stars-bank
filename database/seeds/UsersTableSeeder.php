<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
//use TCG\Voyager\Models\Role;
use TCG\Voyager\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        if (User::count() == 0) {
//            $role = Role::where('name', 'admin')->firstOrFail();

            User::create([
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'address'          => 'г.Москва, ул.Московская 1-1',
                'ogrn'          => '1234567891234',
                'fio'          => 'Иванов Иван Иванович',
                'phone'          => '+7123456789',
                'avatar'          => 'users/default.png',
                'password'       => bcrypt('password'),
                'remember_token' => Str::random(60),
//                'role_id'        => $role->id,
            ]);
        }
    }
}
