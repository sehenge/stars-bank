<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Star;

class StarsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        if (Star::count() == 0) {
            Star::create([
                'name'           => 'Полина Гагарина',
                'price'          => '9100',
                'duration'       => '2-3 дня',
                'description' => Str::random(200),
                'image'        => 'assets/img/images/p1.jpg',
            ]);
        }
    }
}
