<div class="col-lg-3 col-md-4">
    <aside class="page-sidebar page-sidebar--cabinet">
        <div class="setting-list">
            @if (Route::currentRouteName() == 'cabinet.profile')
                <div class="setting-list__item active">
                    <a class="setting-link" href="{{ route('cabinet.profile') }}">Мой профиль</a>
                </div>
            @else
                <div class="setting-list__item">
                    <a class="setting-link" href="{{ route('cabinet.profile') }}">Мой профиль</a>
                </div>
            @endif
            @if (Route::currentRouteName() == 'cabinet.cart')
                <div class="setting-list__item active">
                    <a class="setting-link" href="{{ route('cabinet.cart') }}">Моя корзина</a>
                </div>
            @else
                <div class="setting-list__item">
                    <a class="setting-link" href="{{ route('cabinet.cart') }}">Моя корзина</a>
                </div>
            @endif
            @if (Route::currentRouteName() == 'cabinet.history')
                <div class="setting-list__item active">
                    <a class="setting-link" href="{{ route('cabinet.history') }}">История заказов</a>
                </div>
            @else
                <div class="setting-list__item">
                    <a class="setting-link" href="{{ route('cabinet.history') }}">История заказов</a>
                </div>
            @endif
            @if (Route::currentRouteName() == 'cabinet.verification')
                <div class="setting-list__item active">
                    <a class="setting-link" href="{{ route('cabinet.verification') }}">Верификация</a>
                </div>
            @else
                <div class="setting-list__item">
                    <a class="setting-link" href="{{ route('cabinet.verification') }}">Верификация</a>
                </div>
            @endif
        </div>
    </aside>
</div>