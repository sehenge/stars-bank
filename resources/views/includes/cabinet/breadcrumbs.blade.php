<div class="content-page-top">
    <nav class="page-navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('index') }}">Главная</a></li>
            <li class="breadcrumb-item active">Личный кабинет</li>
        </ol>
    </nav>
    <h1 class="page-title">Личный кабинет</h1>
</div>
