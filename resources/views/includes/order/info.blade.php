<div class="annotation-block">
    <div class="annotation-block__top">
        <h2 class="annotation-block__title">ИНФОРМАЦИЯ О ЗАКАЗЕ</h2>
    </div>
    <div class="annotation-block__middle">
        <div class="result-order">
            <div class="result-order__avatar"><img class="img-fluid" src="/assets/img/images/avatar-star.png" alt=""></div>
            <div class="result-order__text">{{ $star->description }}</div>
        </div>
    </div>
    <div class="annotation-block__bottom">
        <div class="block-descr">
            <div class="card-descr">
                <div class="card-descr__item">
                    <svg class="svg-icon icon-clock ">
                        <use xlink:href="/assets/img/spriteSvg.svg#sprite-clock"></use>
                    </svg>Срок выполнения: <b>{{ $star->duration }}</b>
                </div>
                <div class="card-descr__item">
                    <svg class="svg-icon icon-label ">
                        <use xlink:href="/assets/img/spriteSvg.svg#sprite-label"></use>
                    </svg>Стоимость: <b class="price">{{ $star->price }} ₽</b>
                    <input type="hidden" value="{{ $star->price }}" name="price"/>
                    <input type="hidden" value="{{ $star->id }}" name="star_id"/>
                </div>
            </div>
        </div>
        <div class="form-button">
            <button class="btn btn--long btn-main button" type="submit">Добавить в корзину</button>
        </div>
    </div>
</div>
