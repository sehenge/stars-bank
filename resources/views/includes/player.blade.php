<div class="player-block">
    <button class="btn btn-close" id="close-btn" type="button">
        <svg class="svg-icon icon-close ">
            <use xlink:href="/assets/img/spriteSvg.svg#sprite-close"></use>
        </svg>
    </button>
    <div class="container">
        <div class="row player">
            <div class="col-xl-4 col-lg-4 col-sm-12 player__col">
                <div class="player__unit">
                    <div class="player__avatar" id="track-avatar"><img src="/assets/img/images/author.png" alt=""></div>
                    <div class="player__descr">
                        <div class="player__title" id="track-author">Здесь безумно длинное название песни</div>
                        <div class="player__author" id="track-name">Здесь безумно длинное имя композитора</div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-sm-9 player__col">
                <div class="player__container">
                    <div class="player-navigation">
                        <div class="player-navigation__buttons">
                            <button class="btn btn-nav" id="btnPrev" type="button">
                                <svg class="svg-icon icon-prev ">
                                    <use xlink:href="/assets/img/spriteSvg.svg#sprite-prev"></use>
                                </svg>
                            </button>
                            <button class="btn play-btn button" id="btnPlay" type="button">
                                <svg class="svg-icon icon-play ">
                                    <use xlink:href="/assets/img/spriteSvg.svg#sprite-play"></use>
                                </svg><span class="pause-icon">
                          <svg class="svg-icon icon-pause ">
                            <use xlink:href="/assets/img/spriteSvg.svg#sprite-pause"></use>
                          </svg></span>
                            </button>
                            <button class="btn btn-nav" id="btnNext" type="button">
                                <svg class="svg-icon icon-next ">
                                    <use xlink:href="/assets/img/spriteSvg.svg#sprite-next"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                    <div class="player-body">
                        <audio class="js-player" id="player" controls="" src="/mp3/bensound-dubstep.mp3"></audio>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-sm-3 player__col">
                <div class="player__btns">
                    <button class="btn btn-like" type="button">
                        <svg class="svg-icon icon-star ">
                            <use xlink:href="/assets/img/spriteSvg.svg#sprite-star"></use>
                        </svg>
                    </button>
                    <button class="btn btn-cart button" type="button">
                        <svg class="svg-icon icon-shopping-cart ">
                            <use xlink:href="/assets/img/spriteSvg.svg#sprite-shopping-cart"></use>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
