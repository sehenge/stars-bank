<footer class="footer footer-main">
    <div class="container">
        <div class="footer-top">
            <div class="form-row align-items-center">
                <div class="col-lg-4">
                    <div class="form-row align-items-center mb-4 justify-content-center justify-content-md-start">
                        <div class="col-auto">
                            <div class="logo-wrap logo-wrap--footer">
                                <a class="logo" href="{{ route('index') }}">
                                    <img class="img-fluid logo__img" src="/assets/img/logo.svg" alt="logo" width="52" height="52">
                                    <span class="logo__text">Stars <br>Bank</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="company-info company-info--footer">Новый формат <br>аудиорекламы</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <nav class="footer-menu mb-2">
                        <ul>
                            <li><a href="{{ route('index') }}">Главная</a></li>
                            <li><a href="{{ route('cabinet.profile') }}">Личный кабинет</a></li>
                            <li><a href="{{ route('cabinet.cart') }}">Корзина</a></li>
                            <li><a href="{{ route('contacts') }}">Контакты</a></li>
                            <li><a href="https://lcd.agency/">LC.Design</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="copyright">
                <p>© Stars Bank. 2020. Все права защищены</p>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/assets/js/vendors.0ca1143c34fb340b9f4a.js"></script>
    <script type="text/javascript" src="/assets/js/app.0ca1143c34fb340b9f4a.js"></script>
    <!-- <script type="text/javascript" src="/assets/js/main.js"></script> -->
    <script>
        (function() {
            let elements = document.querySelectorAll('img[data-src]');
            let index = 0;
            let lazyLoad = function() {
                if(index >= elements.length) return;
                let item = elements[index];
                if((this.scrollY + this.innerHeight) > item.offsetTop) {
                    let src = item.getAttribute("data-src");
                    item.src = src;
                    item.addEventListener('load', function() {
                        item.removeAttribute('data-src');
                    });
                    index++;
                    lazyLoad();
                }
            };
            let init = function() {
                window.addEventListener('scroll', lazyLoad);
                lazyLoad();
            };
            return init();
        })();

        jQuery(".remove-from-cart").click(function (e) {
            e.preventDefault();
            var ele = jQuery(this);
            console.log(ele.attr("data-id"));

            if(confirm("Вы уверены?")) {
                jQuery.ajax({
                    url: "{{ route('order.removefromcart') }}",
                    method: "DELETE",
                    data: {
                        _token: '{{ csrf_token() }}', 
                        id: ele.attr("data-id")
                    },
                    success: function (response) {
                        window.location.reload();
                    }
                });
            }
        }); 
    </script>
    
</footer>

