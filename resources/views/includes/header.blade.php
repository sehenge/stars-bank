<header class="header header-main">
    <div class="header-top">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-8 col-md-7 col-auto">
                    <div class="form-row align-items-center">
                        <div class="col-xl-5 col-lg-6 col-auto">
                            <div class="form-row align-items-center">
                                <div class="col-auto">
                                    <div class="logo-wrap">
                                        <a class="logo" href="{{ route('index') }}">
                                            <img class="img-fluid logo__img" src="/assets/img/logo.svg" alt="logo" width="52" height="52">
                                            <span class="logo__text">Stars <br>Bank</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <div class="company-info">Новый формат <br>аудиорекламы</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-6 col-auto">
                            <div class="search-block">
                                <button class="btn search-button-mobile js-m-search-btn" type="button">
                                    <svg class="svg-icon icon-searcher ">
                                        <use xlink:href="/assets/img/spriteSvg.svg#sprite-searcher"></use>
                                    </svg>
                                </button>
                                <form class="form-search" method="get" action="#">
                                    <div class="search-field">
                                        <input class="form-control input-form" type="search" name="s" placeholder="Поиск">
                                        <button class="btn btn-search" type="submit">
                                            <svg class="svg-icon icon-searcher ">
                                                <use xlink:href="/assets/img/spriteSvg.svg#sprite-searcher"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 col-auto ml-auto">
                    <div class="user-block">
                        <div class="user-info">
                            <div class="user-info-list">
                                <div class="user-info-list__item"><a class="user-info-link" href="{{ route('cabinet.cart') }}">
                                        <svg class="svg-icon icon-shopping-cart ">
                                            <use xlink:href="/assets/img/spriteSvg.svg#sprite-shopping-cart"></use>
                                        </svg></a></div>
                                <div class="user-info-list__item"><a class="user-info-link" href="#">
                                        <div class="indicator-circle"></div>
                                        <svg class="svg-icon icon-notifications ">
                                            <use xlink:href="/assets/img/spriteSvg.svg#sprite-notifications"></use>
                                        </svg></a></div>
                                <div class="user-info-list__item"><a class="user-info-link inactive" href="#">
                                        <svg class="svg-icon icon-star ">
                                            <use xlink:href="/assets/img/spriteSvg.svg#sprite-star"></use>
                                        </svg></a></div>
                            </div>
                        </div>
                        <div class="user-setting">
                            @auth
                                <div class="user-setting__avatar js-setting-button">
                                    <img class="img-fluid" src="/assets/img/images/avatar65.jpg" alt="avatar" width="65" height="65">
                                    <svg class="svg-icon icon-down-arrow ">
                                        <use xlink:href="/assets/img/spriteSvg.svg#sprite-down-arrow"></use>
                                    </svg>
                                </div>
                                <div class="user-setting__dropdown js-setting-dropdown">
                                    <div class="dropdown-inner">
                                        <div class="setting-list">
                                            <div class="setting-list__item">
                                                <a class="setting-link" href="{{ route('cabinet.profile') }}">
                                                    <svg class="svg-icon icon-user ">
                                                        <use xlink:href="/assets/img/spriteSvg.svg#sprite-user"></use>
                                                    </svg>Личный кабинет
                                                </a>
                                            </div>
                                            <!--div class="setting-list__item">
                                                <a class="setting-link" href="{{ route('voyager.dashboard') }}">
                                                    <svg class="svg-icon icon-user ">
                                                        <use xlink:href="/assets/img/spriteSvg.svg#sprite-user"></use>
                                                    </svg>Admin panel
                                                </a>
                                            </div-->
                                            <div class="setting-list__item">
                                                <a class="setting-link" href="{{ route('cabinet.cart') }}">
                                                    <svg class="svg-icon icon-shopping-cart ">
                                                        <use xlink:href="/assets/img/spriteSvg.svg#sprite-shopping-cart"></use>
                                                    </svg>Моя корзина</a>
                                            </div>
                                            <div class="setting-list__item">
                                                <a class="setting-link" href="{{ route('cabinet.history') }}">
                                                    <svg class="svg-icon icon-sand-clock ">
                                                        <use xlink:href="/assets/img/spriteSvg.svg#sprite-sand-clock"></use>
                                                    </svg>История покупок</a>
                                            </div>
                                        </div>

                                        <div class="user-info-mobile">
                                            <div class="user-info-list">
                                                <div class="user-info-list__item">
                                                    <a class="user-info-link" href="{{ route('cabinet.cart') }}">
                                                        <svg class="svg-icon icon-shopping-cart ">
                                                            <use xlink:href="/assets/img/spriteSvg.svg#sprite-shopping-cart"></use>
                                                        </svg>
                                                    </a>
                                                </div>
                                                <div class="user-info-list__item">
                                                    <a class="user-info-link" href="#">
                                                        <div class="indicator-circle"></div>
                                                        <svg class="svg-icon icon-notifications ">
                                                            <use xlink:href="/assets/img/spriteSvg.svg#sprite-notifications"></use>
                                                        </svg>
                                                    </a>
                                                </div>
                                                <div class="user-info-list__item">
                                                    <a class="user-info-link inactive" href="#">
                                                        <svg class="svg-icon icon-star ">
                                                            <use xlink:href="/assets/img/spriteSvg.svg#sprite-star"></use>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="setting-dropdown-bottom">
                                            <a class="setting-link" href="{{ route('contacts') }}">Обратная связь</a>
                                        </div>
                                        <div class="setting-dropdown-bottom">
                                            <a class="setting-link" href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                {{ __('Выйти') }}
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endauth

                            @guest
                                <div class="user-setting__avatar js-setting-button">
                                    <img class="img-fluid" src="/assets/img/images/users.svg" alt="avatar" width="65" height="65">
                                    <svg class="svg-icon icon-down-arrow ">
                                        <use xlink:href="/assets/img/spriteSvg.svg#sprite-down-arrow"></use>
                                    </svg>
                                </div>
                                <div class="user-setting__dropdown js-setting-dropdown">
                                    <div class="dropdown-inner">
                                        <div class="setting-list">
                                            <div class="setting-list__item">
                                                <a class="setting-link" href="{{ route('login') }}">
                                                    <svg class="svg-icon icon-user ">
                                                        <use xlink:href="/assets/img/spriteSvg.svg#sprite-user"></use>
                                                    </svg>
                                                    Войти
                                                </a>
                                            </div>

                                            <div class="setting-list__item">
                                                <a class="setting-link" href="{{ route('register') }}">
                                                    <svg class="svg-icon icon-user ">
                                                        <use xlink:href="/assets/img/spriteSvg.svg#sprite-user"></use>
                                                    </svg>
                                                    Регистрация
                                                </a>
                                            </div>
                                        </div>
                                        <div class="user-info-mobile">
                                            <div class="user-info-list">
                                                <div class="user-info-list__item">
                                                    <a class="user-info-link" href="{{ route('login') }}">
                                                        <svg class="svg-icon icon-shopping-cart ">
                                                            <use xlink:href="/assets/img/spriteSvg.svg#sprite-user"></use>
                                                        </svg>
                                                        Войти
                                                    </a>
                                                </div>
                                                <div class="user-info-list__item">
                                                    <a class="user-info-link" href="{{ route('register') }}">
                                                        <svg class="svg-icon icon-shopping-cart ">
                                                            <use xlink:href="/assets/img/spriteSvg.svg#sprite-user"></use>
                                                        </svg>
                                                        Регистрация
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endguest
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="search-mobile js-search-form">
        <div class="container">
            <form class="form-search" method="get" action="#">
                <div class="search-field">
                    <input class="form-control input-form" type="search" name="s" placeholder="Поиск">
                    <button class="btn btn-search" type="submit">
                        <svg class="svg-icon icon-searcher ">
                            <use xlink:href="/assets/img/spriteSvg.svg#sprite-searcher"></use>
                        </svg>
                    </button>
                </div>
            </form>
        </div>
    </div>
</header>