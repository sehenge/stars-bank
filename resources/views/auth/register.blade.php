@extends('layouts.app')

@section('content')
    <main class="content page-content">
        <div class="container">
            <section class="section-main">
                @if(count($errors) > 0 )
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul class="p-0 m-0" style="list-style: none;">
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="form-registration form-login--reg" id="vform" action="{{ route('register') }}" method="POST" onsubmit="return Validate()">
                    @csrf
                    <div class="row">
                        <div class="col-md-7">
                            <div class="content-page-top">
                                <nav class="page-navigation">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{ route('index') }}">Главная</a></li>
                                        <li class="breadcrumb-item active">Регистрация</li>
                                    </ol>
                                </nav>
                                <h1 class="page-title">Регистрация</h1>
                                <p class="section-descr">Внимание! Регистрация доступна только Юридическим лицам и
                                    ИП</p>
                            </div>
                            <div class="form-block">
                                <div class="form-item" id="name_div">
                                    <label class="form-label" for="name">Наименование компании / ИП</label>
                                    <input class="form-input" type="text" name="name" id="name">
                                </div>
                                <div class="form-item" id="address_div">
                                    <label class="form-label" for="address">Юр адрес или фактический адрес ИП</label>
                                    <input class="form-input" type="text" name="address" id="address">
                                </div>
                                <div class="form-item" id="ogrn_div">
                                    <label class="form-label" for="ogrn">ОГРН / ОГРНИП</label>
                                    <input class="form-input" type="text" name="ogrn" id="ogrn"
                                           minlength="13" maxlength="13" onkeypress="return isNumberKey(event)">
                                </div>
                                <div class="form-item" id="ti_number_div">
                                    <label class="form-label" for="ti_number">ИНН</label>
                                    <input class="form-input" type="text" name="ti_number" id="ti_number"
                                           minlength="12" maxlength="12" onkeypress="return isNumberKey(event)">
                                </div>
                                <div class="form-item" id="fio_div">
                                    <label class="form-label" for="fio">ФИО представителя</label>
                                    <input class="form-input" type="text" name="fio" id="fio">
                                </div>
                                <div class="form-item" id="email_div">
                                    <label class="form-label" for="email">Email</label>
                                    <input class="form-input" type="email" name="email" id="email">
                                </div>
                                <div class="form-item" id="phone_div">
                                    <label class="form-label" for="phone">Номер телефона</label>
                                    <input class="form-input" type="text" name="phone" id="phone" onkeypress="return isNumberKey(event)">
                                </div>
                                <div class="form-item" id="password_div">
                                    <label class="form-label" for="password">Пароль</label>
                                    <input class="form-input" type="password" name="password" id="password"
                                           placeholder="Пароль">
                                </div>
                                <div class="form-item" id="password_confirmation_div">
                                    <label class="form-label" for="password_confirmation">Подтверждение пароля</label>
                                    <input class="form-input" type="password" name="password_confirmation" id="password_confirmation"
                                           placeholder="Подтверждение пароля">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="annotation-block">
                                <div class="annotation-block__top">
                                    <h2 class="annotation-block__title">Порядок регистрации</h2>
                                </div>
                                <div class="annotation-block__middle">
                                    <ul class="checked-list">
                                        <li class="checked-list__item" id="name_check">
                                            <i class="icon-check">
                                                <svg class="svg-icon icon-check ">
                                                    <use xlink:href="/assets/img/spriteSvg.svg#sprite-check"></use>
                                                </svg>
                                            </i>Наименование компании / ИП
                                        </li>
                                        <li class="checked-list__item" id="address_check"><i class="icon-check">
                                                <svg class="svg-icon icon-check ">
                                                    <use xlink:href="/assets/img/spriteSvg.svg#sprite-check"></use>
                                                </svg>
                                            </i>Юр адрес или фактический адрес ИП
                                        </li>
                                        <li class="checked-list__item" id="ogrn_check"><i class="icon-check">
                                                <svg class="svg-icon icon-check ">
                                                    <use xlink:href="/assets/img/spriteSvg.svg#sprite-check"></use>
                                                </svg>
                                            </i>ОГРН или ОГРНИП
                                        </li>
                                        <li class="checked-list__item" id="ti_number_check"><i class="icon-check">
                                                <svg class="svg-icon icon-check ">
                                                    <use xlink:href="/assets/img/spriteSvg.svg#sprite-check"></use>
                                                </svg>
                                            </i>ИНН
                                        </li>
                                        <li class="checked-list__item" id="fio_check"><i class="icon-check">
                                                <svg class="svg-icon icon-check ">
                                                    <use xlink:href="/assets/img/spriteSvg.svg#sprite-check"></use>
                                                </svg>
                                            </i>ФИО представителя
                                        </li>
                                        <li class="checked-list__item" id="email_check"><i class="icon-check">
                                                <svg class="svg-icon icon-check ">
                                                    <use xlink:href="/assets/img/spriteSvg.svg#sprite-check"></use>
                                                </svg>
                                            </i>Email
                                        </li>
                                        <li class="checked-list__item" id="phone_check"><i class="icon-check">
                                                <svg class="svg-icon icon-check ">
                                                    <use xlink:href="/assets/img/spriteSvg.svg#sprite-check"></use>
                                                </svg>
                                            </i>Номер телефона
                                        </li>
                                        <li class="checked-list__item" id="password_check"><i class="icon-check">
                                                <svg class="svg-icon icon-check ">
                                                    <use xlink:href="/assets/img/spriteSvg.svg#sprite-check"></use>
                                                </svg>
                                            </i>Пароль
                                        </li>
                                        <li class="checked-list__item" id="password_confirmation_check"><i class="icon-check">
                                                <svg class="svg-icon icon-check ">
                                                    <use xlink:href="/assets/img/spriteSvg.svg#sprite-check"></use>
                                                </svg>
                                            </i>Подтверждение пароль
                                        </li>
                                    </ul>
                                </div>
                                <div class="annotation-block__bottom">
                                    <div class="checkbox-item checkbox-item--term">
                                        <input type="checkbox" name="check" id="rd1" checked
                                               data-msg-required="Необходимо соглашение на условия" required>
                                        <label for="rd1">
                                            <span class="check-icon"></span>
                                            <span class="check-text">Я согласен с условиями обработки 
                                                <a href="{{ route('policy') }}" target="_blank">Персональных данных</a>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-button">
                                        <button class="btn btn--long btn-main button" type="submit">Зарегистрироваться</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </section>
            <script>
                let name = document.getElementById('name');
                let address = document.getElementById('address');
                let ogrn = document.getElementById('ogrn');
                let ti_number = document.getElementById('ti_number');
                let fio = document.getElementById('fio');
                let email = document.getElementById('email');
                let phone = document.getElementById('phone');
                let password = document.getElementById('password');
                let password_confirmation = document.getElementById('password_confirmation');

                /*var name_error = document.getElementById('name_error');
                var email_error = document.getElementById('email_error');
                var password_error = document.getElementById('password_error');*/

                name.addEventListener('blur', nameVerify, true);
                address.addEventListener('blur', addressVerify, true);
                ogrn.addEventListener('blur', ogrnVerify, true);
                ti_number.addEventListener('blur', tinumberVerify, true);
                fio.addEventListener('blur', fioVerify, true);
                email.addEventListener('blur', emailVerify, true);
                phone.addEventListener('blur', phoneVerify, true);
                password.addEventListener('blur', passwordVerify, true);
                password_confirmation.addEventListener('blur', passwordconfirmationVerify, true);

                function isNumberKey(evt)
                {
                    var charCode = (evt.which) ? evt.which : event.keyCode;
                    if (charCode != 46 && charCode > 31 
                        && (charCode < 48 || charCode > 57))
                        return false;

                    return true;
                }
                
                function Validate() {
                    if (name.value == "") {
                        name.style.border = "1px solid red";
                        document.getElementById('name_div').style.color = "red";
                        name.focus();
                        return false;
                    }   
                    if (address.value == "") {
                        address.style.border = "1px solid red";
                        document.getElementById('address_div').style.color = "red";
                        address.focus();
                        return false;
                    }   
                    if (ogrn.value == "") {
                        ogrn.style.border = "1px solid red";
                        document.getElementById('ogrn_div').style.color = "red";
                        ogrn.focus();
                        return false;
                    }   
                    if (ti_number.value == "") {
                        ti_number.style.border = "1px solid red";
                        document.getElementById('ti_number_div').style.color = "red";
                        ti_number.focus();
                        return false;
                    }   
                    if (fio.value == "") {
                        fio.style.border = "1px solid red";
                        document.getElementById('fio_div').style.color = "red";
                        fio.focus();
                        return false;
                    }                        
                    if (email.value == "") {
                        email.style.border = "1px solid red";
                        document.getElementById('email_div').style.color = "red";
                        // email_error.textContent = "Email обязателен для заполнения";
                        email.focus();
                        return false;
                    }   
                    if (phone.value == "") {
                        phone.style.border = "1px solid red";
                        document.getElementById('phone_div').style.color = "red";
                        phone.focus();
                        return false;
                    } 
                    if (password.value == "") {
                        password.style.border = "1px solid red";
                        document.getElementById('password_div').style.color = "red";
                        password_confirmation.style.border = "1px solid red";
                        // password_error.textContent = "Пароль обязателен для заполнения";
                        password.focus();
                        return false;
                    }

                    // check if the two passwords match
                    if (password.value != password_confirmation.value) {
                        password.style.border = "1px solid red";
                        document.getElementById('password_confirmation_div').style.color = "red";
                        password_confirmation.style.border = "1px solid red";
                        // password_error.innerHTML = "Пароли не совпадают";
                        return false;
                    }
                    
                }

                // event handler functions
                function nameVerify() {
                    if (name.value != "") {
                        console.log('not empty');
                        name.style.border = "1px solid #5e6e66";
                        document.getElementById('name_div').style.color = "#5e6e66";
                        document.getElementById('name_check').style.opacity = "1";
                        return true;
                    } else {
                        name.style.border = "1px solid red";
                        document.getElementById('name_check').style.opacity = "0.5";
                    }
                }

                function addressVerify() {
                    if (address.value != "") {
                        address.style.border = "1px solid #5e6e66";
                        document.getElementById('address_div').style.color = "#5e6e66";
                        document.getElementById('address_check').style.opacity = "1";
                        return true;
                    } else {
                        address.style.border = "1px solid red";
                        document.getElementById('address_check').style.opacity = "0.5";
                    }
                }

                function ogrnVerify() {
                    if (ogrn.value != "") {
                        ogrn.style.border = "1px solid #5e6e66";
                        document.getElementById('ogrn_div').style.color = "#5e6e66";
                        document.getElementById('ogrn_check').style.opacity = "1";
                        return true;
                    } else {
                        ogrn.style.border = "1px solid red";
                        document.getElementById('ogrn_check').style.opacity = "0.5";
                    }
                }

                function tinumberVerify() {
                    if (ti_number.value != "") {
                        ti_number.style.border = "1px solid #5e6e66";
                        document.getElementById('ti_number_div').style.color = "#5e6e66";
                        document.getElementById('ti_number_check').style.opacity = "1";
                        return true;
                    } else {
                        ti_number.style.border = "1px solid red";
                        document.getElementById('ti_number_check').style.opacity = "0.5";
                    }
                }

                function fioVerify() {
                    if (fio.value != "") {
                        fio.style.border = "1px solid #5e6e66";
                        document.getElementById('fio_div').style.color = "#5e6e66";
                        document.getElementById('fio_check').style.opacity = "1";
                        return true;
                    } else {
                        fio.style.border = "1px solid red";
                        document.getElementById('fio_check').style.opacity = "0.5";
                    }
                }

                function emailVerify() {
                    if (email.value != "") {
                        email.style.border = "1px solid #5e6e66";
                        document.getElementById('email_div').style.color = "#5e6e66";
                        document.getElementById('email_check').style.opacity = "1";  
                        // email_error.innerHTML = "";
                        return true;
                    } else {
                        email.style.border = "1px solid red";
                        document.getElementById('email_check').style.opacity = "0.5";
                    }
                }

                function phoneVerify() {
                    if (phone.value != "") {
                        phone.style.border = "1px solid #5e6e66";
                        document.getElementById('phone_div').style.color = "#5e6e66";
                        document.getElementById('phone_check').style.opacity = "1";
                        return true;
                    } else {
                        phone.style.border = "1px solid red";
                        document.getElementById('phone_check').style.opacity = "0.5";
                    }
                }

                function passwordVerify() {
                    if (password.value != "") {
                        password.style.border = "1px solid #5e6e66";
                        document.getElementById('password_div').style.color = "#5e6e66";
                        document.getElementById('password_check').style.opacity = "1";
                        return true;
                    } else {
                        password.style.border = "1px solid red";
                        document.getElementById('password_check').style.opacity = "0.5";
                    }
                }

                function passwordconfirmationVerify() {
                    if (password_confirmation.value != "") {
                        password_confirmation.style.border = "1px solid #5e6e66";
                        document.getElementById('password_confirmation_div').style.color = "#5e6e66";
                        document.getElementById('password_confirmation_check').style.opacity = "1";
                        return true;
                    } else {
                        password_confirmation.style.border = "1px solid red";
                        document.getElementById('password_confirmation_check').style.opacity = "0.5";
                    }
                }
            </script>
            <style>
            .checked-list__item .icon-check {
                -webkit-transform: scale(1);
                opacity: inherit;
            }
            </style>
        </div>
    </main>
@endsection
