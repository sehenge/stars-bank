@extends('layouts.app')

@section('content')
    <main class="content form-content">
        <div class="container">
            <div class="center-block center-block--form">
                @if(count($errors) > 0 )
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul class="p-0 m-0" style="list-style: none;">
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h1 class="center-block__title">Авторизация</h1>
                <form class="form-login form-login--reg" action="{{ route('loginpost') }}" method="POST">
                    @csrf
                    <div class="form-item">
                        <label class="form-label" for="username">Ваш Логин (или ИНН):</label>
                        <input class="form-input" type="text" name="email" id="login"
                               placeholder="Логин или ИНН">
                    </div>
                    <div class="form-item">
                        <label class="form-label" for="password">Пароль:</label>
                        <input class="form-input" type="password" name="password" id="password" placeholder="Пароль">
                    </div>
                    <div class="form-item">
                        <input class="form-checkbox" type="checkbox" name="remember" id="remember" />
                        Запомнить
                    </div>
                    <div class="form-bottom row justify-content-between align-items-center">
                        <div class="col-auto form-bottom__item">
                            <div class="form-button">
                                <button class="btn btn-main button" type="submit">Войти</button>
                            </div>
                        </div>
                        <div class="col-auto ml-auto form-bottom__item"><a class="link"
                                                                           href="{{ route('password.request') }}">Забыли
                                пароль?</a></div>
                        <div class="col-auto ml-auto form-bottom__item"><a class="link"
                                                                           href="{{ route('register') }}">Регистрация</a></div>
                    </div>
                </form>
            </div>
        </div>
    </main>
@endsection
