@extends('layouts.master')

@section('content')
    <main class="content page-content">
        <div class="container">
            <section class="section-main">
                <div class="content-page-top">
                    <div class="row">
                        <div class="col-md-auto">
                            <nav class="page-navigation">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('index') }}">Главная</a></li>
                                    <li class="breadcrumb-item active">Список звезд</li>
                                </ol>
                            </nav>
                            <h1 class="page-title">Список звезд</h1>
                        </div>
                        <div class="col-md-auto ml-md-auto">
                            <nav class="page-filter">
                                <ul class="page-filter-list form-row align-items-center">
                                    <li class="col-sm-auto page-filter-list__item active"><a class="btn filter-btn button" href="#">Сначала популярные</a></li>
                                    <li class="col-sm-auto page-filter-list__item"><a class="btn filter-btn button" href="#">Сначала новые</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-4">
                        <aside class="page-sidebar">
                            <div class="widget">
                                <h3 class="widget__title">Звезды по направлениям:</h3>
                                <div class="widget-filter">
                                    <div class="check-list">
                                        <div class="check-list__item">
                                            <div class="checkbox-item">
                                                <input type="checkbox" id="ch1" value="Все направления">
                                                <label for="ch1"><span class="check-icon"></span><span class="check-text">Все направления</span></label>
                                            </div>
                                        </div>
                                        <div class="check-list__item">
                                            <div class="checkbox-item">
                                                <input type="checkbox" id="ch2" value="Музыканты">
                                                <label for="ch2"><span class="check-icon"></span><span class="check-text">Музыканты</span></label>
                                            </div>
                                        </div>
                                        <div class="check-list__item">
                                            <div class="checkbox-item">
                                                <input type="checkbox" id="ch3" value="Ведущие">
                                                <label for="ch3"><span class="check-icon"></span><span class="check-text">Ведущие</span></label>
                                            </div>
                                        </div>
                                        <div class="check-list__item">
                                            <div class="checkbox-item">
                                                <input type="checkbox" id="ch4" value="Актеры">
                                                <label for="ch4"><span class="check-icon"></span><span class="check-text">Актеры</span></label>
                                            </div>
                                        </div>
                                        <div class="check-list__item">
                                            <div class="checkbox-item">
                                                <input type="checkbox" id="ch5" value="Блогеры">
                                                <label for="ch5"><span class="check-icon"></span><span class="check-text">Блогеры</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="widget">
                                <h3 class="widget__title mb-0">Стоимость:</h3>
                                <div class="filter-range">
                                    <input class="js-range-slider" type="text" name="range-price" data-min="0" data-max="20000" data-step="1" data-grid="false" data-type="double" data-hide-min-max="true" data-hide-from-to="true">
                                </div>
                                <div class="extra-controls">
                                    <div class="row">
                                        <div class="col-6">
                                            <input class="form-input js-input-from js-number-field" type="text" value="0">
                                        </div>
                                        <div class="col-6">
                                            <input class="form-input js-input-to js-number-field" type="text" value="0">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="widget">
                                <h3 class="widget__title">Вид заказа:</h3>
                                <div class="widget-filter">
                                    <div class="check-list">
                                        <div class="check-list__item">
                                            <div class="checkbox-item">
                                                <input type="checkbox" id="chk1" value="Все виды">
                                                <label for="chk1"><span class="check-icon"></span><span class="check-text">Все виды</span></label>
                                            </div>
                                        </div>
                                        <div class="check-list__item">
                                            <div class="checkbox-item">
                                                <input type="checkbox" id="chk2" value="Реклама">
                                                <label for="chk2"><span class="check-icon"></span><span class="check-text">Реклама</span></label>
                                            </div>
                                        </div>
                                        <div class="check-list__item">
                                            <div class="checkbox-item">
                                                <input type="checkbox" id="chk3" value="Поздравление">
                                                <label for="chk3"><span class="check-icon"></span><span class="check-text">Поздравление</span></label>
                                            </div>
                                        </div>
                                        <div class="check-list__item">
                                            <div class="checkbox-item">
                                                <input type="checkbox" id="chk4" value="Аудиосообщение">
                                                <label for="chk4"><span class="check-icon"></span><span class="check-text">Аудиосообщение</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="button-filter">
                                    <button class="btn btn-main-g" type="button">Применить</button>
                                </div>
                            </div>
                        </aside>
                    </div>
                    <div class="col-lg-9 col-md-8">
                        <div class="row product-list">
                            <div class="product-list__item col-lg-4 col-sm-6 js-card-wrap">
                                @foreach ($stars as $star)
                                    <div class="product-card js-card" data-track="/mp3/bensound-acousticbreeze.mp3" data-index="" data-title="Аудио реклама" data-author="Полина Гагарина" data-avatar="/assets/img/images/avatar65.jpg" data-like="true">
                                        <div class="product-card__image">
                                            <a href="{{ route('card.product', ['id' => $star->id]) }}">
                                                <img class="img-fluid" src="/{{ $star->image }}" alt="">
                                            </a>
                                            <button class="btn btn-like active" type="button">
                                                <svg class="svg-icon icon-star ">
                                                    <use xlink:href="/assets/img/spriteSvg.svg#sprite-star"></use>
                                                </svg>
                                            </button>
                                            <div class="product-card__play">
                                                <button class="btn play-btn button js-play-track" type="button">
                                                    <svg class="svg-icon icon-play ">
                                                        <use xlink:href="/assets/img/spriteSvg.svg#sprite-play"></use>
                                                    </svg><span class="pause-icon">
                                                    <svg class="svg-icon icon-pause ">
                                                      <use xlink:href="/assets/img/spriteSvg.svg#sprite-pause"></use>
                                                    </svg></span>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="product-card__middle">
                                            <div class="product-card__title">
                                                <a href="{{ route('card.product', ['id' => $star->id]) }}">{{ $star->name }}</a>
                                            </div>
                                        </div>
                                        <div class="product-card__bottom">
                                            <div class="separate-line"></div>
                                            <div class="row card-descr">
                                                <div class="col-auto">
                                                    <div class="card-descr__item">
                                                        <svg class="svg-icon icon-clock ">
                                                            <use xlink:href="/assets/img/spriteSvg.svg#sprite-clock"></use>
                                                        </svg>
                                                        {{ $star->duration }}
                                                    </div>
                                                </div>
                                                <div class="col-auto ml-auto">
                                                    <div class="card-descr__item">
                                                        <svg class="svg-icon icon-label ">
                                                            <use xlink:href="/assets/img/spriteSvg.svg#sprite-label"></use>
                                                        </svg>
                                                        {{ $star->price }} ₽
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <nav class="pagination-nav">
                            <ul class="pagination">
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><span class="page-link">...</span></li>
                                <li class="page-item"><a class="page-link" href="#">10</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection
