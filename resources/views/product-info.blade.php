@extends('layouts.master')

@section('content')
    <main class="content page-content">
        <div class="container">
            <section class="section-main">
                <div class="content-page-top">
                    <nav class="page-navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('index') }}">Главная</a></li>
                            <li class="breadcrumb-item active">Оформление заказа</li>
                        </ol>
                    </nav>
                    <h1 class="page-title">Оформление заказа</h1>
                </div>
                <div class="product-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-5 col-sm-6">
                            <div class="product-card product-card--single">
                                <div class="product-card__image product-card__image--full">
                                    <img class="img-fluid js-lazy" data-src="/assets/img/images/star-image.jpg" alt="">
                                </div>
                                <div class="product-card__middle">
                                    <div class="single-player">
                                        <div class="single-player__nav">
                                            <button class="btn play-btn button" id="btnPlay2" type="button">
                                                <svg class="svg-icon icon-play ">
                                                    <use xlink:href="/assets/img/spriteSvg.svg#sprite-play"></use>
                                                </svg><span class="pause-icon">
                                                <svg class="svg-icon icon-pause ">
                                                <use xlink:href="/assets/img/spriteSvg.svg#sprite-pause"></use>
                                                </svg></span>
                                            </button>
                                        </div>
                                        <div class="player-body">
                                            <p class="player-body__title">Полига Гагарина - Аудиореклама Demo</p>
                                            <audio class="js-player" id="player2" controls="" src="/mp3/gagarina.mp3"></audio>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-card__bottom">
                                    <div class="button-order-line"><a class="btn btn--long btn-main-g" href="{{ route('order', ['id' => $star->id]) }}">Заказать</a>
                                        <button class="btn btn-like btn-like--around" type="button">
                                            <svg class="svg-icon icon-star ">
                                                <use xlink:href="/assets/img/spriteSvg.svg#sprite-star"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-7 col-sm-6">
                            <div class="text-block">
                                <h2 class="text-block__title">Информация о звезде:</h2>
                                <p class="text-block__text">Полина Сергеевна Гагарина (род. 27 марта 1987 года, Москва, СССР) — российская певица, актриса кино, телевидения, озвучивания и дубляжа, композитор и фотомодель. Участница и победительница проекта «Первого канала» «Фабрика звёзд-2» (2003). Представительница России на музыкальном конкурсе «Евровидение-2015», занявшая второе место. Полина Сергеевна Гагарина (род. 27 марта 1987 года, Москва, СССР) — российская певица, актриса кино, телевидения, озвучивания и дубляжа, композитор и фотомодель. Участница и победительница проекта «Первого канала» «Фабрика звёзд-2» (2003).</p>
                            </div>
                            <div class="description-block">
                                <div class="description-block__title">Стоимость аудиорекламы:</div>
                                <div class="description-block__list">
                                    <div class="description-list">
                                        <div class="description-list__item">
                                            <div class="form-row align-items-center">
                                                <div class="col-lg-9 col-sm-8"><span>До 1 минуты</span></div>
                                                <div class="col-lg-3 col-sm-4"><b>3 000 ₽</b></div>
                                            </div>
                                        </div>
                                        <div class="description-list__item">
                                            <div class="form-row align-items-center">
                                                <div class="col-lg-9 col-sm-8"><span>От 1 - до 2 минут</span></div>
                                                <div class="col-lg-3 col-sm-4"><b>5 000 ₽</b></div>
                                            </div>
                                        </div>
                                        <div class="description-list__item">
                                            <div class="form-row align-items-center">
                                                <div class="col-lg-9 col-sm-8"><span>От 2 - до 3 минут</span></div>
                                                <div class="col-lg-3 col-sm-4"><b>9 000 ₽</b></div>
                                            </div>
                                        </div>
                                        <div class="description-list__item">
                                            <div class="form-row align-items-center">
                                                <div class="col-lg-9 col-sm-8"><span>От 3 минут</span></div>
                                                <div class="col-lg-3 col-sm-4"><b>Требуется расчет</b></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-page-top">
                    <h2 class="page-title">Вас могут заинтересовать:</h2>
                </div>
                <div class="row product-list">
                    <div class="product-list__item col-lg-3 col-md-4 col-sm-6 js-card-wrap">
                        <div class="product-card js-card" data-track="/mp3/gagarina.mp3" data-index="" data-title="Аудио реклама" data-author="Полина Гагарина" data-avatar="/assets/img/images/avatar65.jpg" data-like="true">
                            <div class="product-card__image"><a href="/card-product"><img class="img-fluid js-lazy" data-src="/assets/img/images/p1.jpg" alt=""></a>
                                <button class="btn btn-like active" type="button">
                                    <svg class="svg-icon icon-star ">
                                        <use xlink:href="/assets/img/spriteSvg.svg#sprite-star"></use>
                                    </svg>
                                </button>
                                <div class="product-card__play">
                                    <button class="btn play-btn button js-play-track" type="button">
                                        <svg class="svg-icon icon-play ">
                                            <use xlink:href="/assets/img/spriteSvg.svg#sprite-play"></use>
                                        </svg><span class="pause-icon">
                            <svg class="svg-icon icon-pause ">
                              <use xlink:href="/assets/img/spriteSvg.svg#sprite-pause"></use>
                            </svg></span>
                                    </button>
                                </div>
                            </div>
                            <div class="product-card__middle">
                                <div class="product-card__title">
                                    <a href="{{ route('card.product', ['id' => $star->id]) }}">Полина Гагарина</a>
                                </div>
                            </div>
                            <div class="product-card__bottom">
                                <div class="separate-line"></div>
                                <div class="row card-descr">
                                    <div class="col-auto">
                                        <div class="card-descr__item">
                                            <svg class="svg-icon icon-clock ">
                                                <use xlink:href="/assets/img/spriteSvg.svg#sprite-clock"></use>
                                            </svg>2-3 дня
                                        </div>
                                    </div>
                                    <div class="col-auto ml-auto">
                                        <div class="card-descr__item">
                                            <svg class="svg-icon icon-label ">
                                                <use xlink:href="/assets/img/spriteSvg.svg#sprite-label"></use>
                                            </svg>9 100 ₽
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product-list__item col-lg-3 col-md-4 col-sm-6 js-card-wrap">
                        <div class="product-card js-card" data-track="/mp3/rozenbaum.mp3" data-index="" data-author="Александр Розенбаум" data-avatar="/assets/img/images/avatar65.jpg" data-like="false">
                            <div class="product-card__image"><a href="{{ route('card.product', ['id' => $star->id]) }}"><img class="img-fluid js-lazy" data-src="/assets/img/images/p2.jpg" alt=""></a>
                                <button class="btn btn-like" type="button">
                                    <svg class="svg-icon icon-star ">
                                        <use xlink:href="/assets/img/spriteSvg.svg#sprite-star"></use>
                                    </svg>
                                </button>
                                <div class="product-card__play">
                                    <button class="btn play-btn button js-play-track" type="button">
                                        <svg class="svg-icon icon-play ">
                                            <use xlink:href="/assets/img/spriteSvg.svg#sprite-play"></use>
                                        </svg><span class="pause-icon">
                            <svg class="svg-icon icon-pause ">
                              <use xlink:href="/assets/img/spriteSvg.svg#sprite-pause"></use>
                            </svg></span>
                                    </button>
                                </div>
                            </div>
                            <div class="product-card__middle">
                                <div class="product-card__title"><a href="{{ route('card.product', ['id' => $star->id]) }}">Александр Розенбаум</a></div>
                            </div>
                            <div class="product-card__bottom">
                                <div class="separate-line"></div>
                                <div class="row card-descr">
                                    <div class="col-auto">
                                        <div class="card-descr__item">
                                            <svg class="svg-icon icon-clock ">
                                                <use xlink:href="/assets/img/spriteSvg.svg#sprite-clock"></use>
                                            </svg>2-3 дня
                                        </div>
                                    </div>
                                    <div class="col-auto ml-auto">
                                        <div class="card-descr__item">
                                            <svg class="svg-icon icon-label ">
                                                <use xlink:href="/assets/img/spriteSvg.svg#sprite-label"></use>
                                            </svg>9 100 ₽
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product-list__item col-lg-3 col-md-4 col-sm-6 js-card-wrap">
                        <div class="product-card js-card" data-track="/mp3/dubcseva.mp3" data-index="" data-title="Аудио реклама" data-author="Ирина дубцова" data-avatar="/assets/img/images/author.png" data-like="false">
                            <div class="product-card__image">
                                <a href="{{ route('card.product', ['id' => $star->id]) }}">
                                    <img class="img-fluid js-lazy" data-src="/assets/img/images/p3.jpg" alt="">
                                </a>
                                <button class="btn btn-like" type="button">
                                    <svg class="svg-icon icon-star ">
                                        <use xlink:href="/assets/img/spriteSvg.svg#sprite-star"></use>
                                    </svg>
                                </button>
                                <div class="product-card__play">
                                    <button class="btn play-btn button js-play-track" type="button">
                                        <svg class="svg-icon icon-play ">
                                            <use xlink:href="/assets/img/spriteSvg.svg#sprite-play"></use>
                                        </svg><span class="pause-icon">
                            <svg class="svg-icon icon-pause ">
                              <use xlink:href="/assets/img/spriteSvg.svg#sprite-pause"></use>
                            </svg></span>
                                    </button>
                                </div>
                            </div>
                            <div class="product-card__middle">
                                <div class="product-card__title"><a href="{{ route('card.product', ['id' => $star->id]) }}">Ирина дубцова</a></div>
                            </div>
                            <div class="product-card__bottom">
                                <div class="separate-line"></div>
                                <div class="row card-descr">
                                    <div class="col-auto">
                                        <div class="card-descr__item">
                                            <svg class="svg-icon icon-clock ">
                                                <use xlink:href="/assets/img/spriteSvg.svg#sprite-clock"></use>
                                            </svg>2-3 дня
                                        </div>
                                    </div>
                                    <div class="col-auto ml-auto">
                                        <div class="card-descr__item">
                                            <svg class="svg-icon icon-label ">
                                                <use xlink:href="/assets/img/spriteSvg.svg#sprite-label"></use>
                                            </svg>9 100 ₽
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product-list__item col-lg-3 col-md-4 col-sm-6 js-card-wrap">
                        <div class="product-card js-card" data-track="/mp3/valeria.mp3" data-index="" data-title="Аудио реклама" data-author="Валерия" data-avatar="/assets/img/images/avatar65.jpg" data-like="false">
                            <div class="product-card__image"><a href="{{ route('card.product', ['id' => $star->id]) }}"><img class="img-fluid js-lazy" data-src="/assets/img/images/p4.jpg" alt=""></a>
                                <button class="btn btn-like" type="button">
                                    <svg class="svg-icon icon-star ">
                                        <use xlink:href="/assets/img/spriteSvg.svg#sprite-star"></use>
                                    </svg>
                                </button>
                                <div class="product-card__play">
                                    <button class="btn play-btn button js-play-track" type="button">
                                        <svg class="svg-icon icon-play ">
                                            <use xlink:href="/assets/img/spriteSvg.svg#sprite-play"></use>
                                        </svg><span class="pause-icon">
                            <svg class="svg-icon icon-pause ">
                              <use xlink:href="/assets/img/spriteSvg.svg#sprite-pause"></use>
                            </svg></span>
                                    </button>
                                </div>
                            </div>
                            <div class="product-card__middle">
                                <div class="product-card__title"><a href="{{ route('card.product', ['id' => $star->id]) }}">Валерия</a></div>
                            </div>
                            <div class="product-card__bottom">
                                <div class="separate-line"></div>
                                <div class="row card-descr">
                                    <div class="col-auto">
                                        <div class="card-descr__item">
                                            <svg class="svg-icon icon-clock ">
                                                <use xlink:href="/assets/img/spriteSvg.svg#sprite-clock"></use>
                                            </svg>2-3 дня
                                        </div>
                                    </div>
                                    <div class="col-auto ml-auto">
                                        <div class="card-descr__item">
                                            <svg class="svg-icon icon-label ">
                                                <use xlink:href="/assets/img/spriteSvg.svg#sprite-label"></use>
                                            </svg>9 100 ₽
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection
