@extends('layouts.cabinet')

@section('content')
    <main class="content page-content">
        <div class="container">
            <section class="section-main">
                @include('includes.cabinet.breadcrumbs')
                <div class="row">
                    @include('includes.cabinet.sidebar')
                    <div class="col-lg-9 col-md-8">
                        @if (Session::has('success'))
                            <div class="alert alert-success">
                                <i class="fas fa-check-circle"></i> {{ Session::get('success') }}
                            </div>
                        @endif
                        @if(count($errors) > 0 )
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <ul class="p-0 m-0" style="list-style: none;">
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="page-block">
                            <div class="top-line">
                                <h2 class="page-block__title">Мой профиль</h2>
                            </div>
                            <div class="block-form">
                                <form class="form-user" action="{{ route('profile.update') }}" method="POST">
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-item">
                                                <label class="form-label" for="name">Наименование компании / ИП:</label>
                                                <input class="form-input" value="{{ $user->name }}"
                                                       type="text" name="name" placeholder="например, ООО &quot;Stars-Bank&quot;" id="name">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-item">
                                                <label class="form-label" for="ogrn">ОГРН/ОГРНИП:</label>
                                                <input class="form-input" value="{{ $user->ogrn }}"
                                                       type="text" name="ogrn" placeholder="ОГРН/ОГРНИП" id="ogrn" pattern="\d*"
                                                       minlength="13" maxlength="13">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-item">
                                                <label class="form-label" for="ti_number">ИНН:</label>
                                                <input class="form-input" value="{{ $user->ti_number }}"
                                                       type="text" name="ti_number" placeholder="ИНН" id="ti_number" pattern="\d*"
                                                       minlength="12" maxlength="12">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-item">
                                                <label class="form-label" for="fio">ФИО представителя:</label>
                                                <input class="form-input"  value="{{ $user->fio }}"
                                                       type="text" name="fio" placeholder="Иванов Алексей Сергеевич" id="fio">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-item">
                                                <label class="form-label" for="email">Email:</label>
                                                <input class="form-input" value="{{ $user->email }}"
                                                       type="email" name="email" placeholder@mail.ru" id="email">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-item">
                                                <label class="form-label" for="phone">Телефон:</label>
                                                <input class="form-input" value="{{ $user->phone }}"
                                                       type="text" name="phone" pattern="\d*"
                                                       placeholder="79999999999" id="phone">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-button">
                                        <button class="btn btn--long btn-main button" type="submit">Применить</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection
