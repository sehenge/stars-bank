@extends('layouts.cabinet')

@section('content')
    <main class="content page-content">
        <div class="container">
            <section class="section-main">
                @include('includes.cabinet.breadcrumbs')
                <div class="row">
                    @include('includes.cabinet.sidebar')
                    <div class="col-lg-9 col-md-8">
                        <div class="page-block">
                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        @endif
                            <form class="form-cart" method="post" action="{{ route('order.place') }}">
                                @csrf                            
                                <div class="top-line">
                                    <div class="row align-items-center">
                                        <div class="col-auto top-line__col">
                                            <h2 class="page-block__title">Моя корзина</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="block-form">
                                    <div class="cart-list">
                                        <?php $total = 0; ?> 
                                        @if(session('cart'))
                                            @foreach(session('cart') as $id => $details)
                                                <?php $total += $details['price'] * $details['quantity'] ?>
                                                <div class="cart-list__item js-cart-item js-card-wrap">
                                                    <div class="js-card">
                                                        <div class="form-row align-items-center">
                                                            <div class="col-sm-1 order-sm-3">
                                                                <div class="form-button form-button--right">
                                                                    <button class="btn btn-delete remove-from-cart" type="button" data-id="{{ $id }}">
                                                                        <svg class="svg-icon icon-close">
                                                                            <use xlink:href="/assets/img/spriteSvg.svg#sprite-close"></use>
                                                                        </svg>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2 hidden-xs">
                                                                <img src="/{{ $details['image'] }}" width="100%" class="img-responsive"/>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="cart-text"><b>Название:</b> {{ $details['adv_name'] }}</div>
                                                                <div class="cart-text"><b>Адрес:</b> {{ $details['adv_address'] }}</div>
                                                                <div class="cart-text"><b>Текст:</b> {{ $details['adv_text'] }}</div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="cart-price cart-price--right">{{ $details['price'] }} ₽</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif                                        
                                    </div>
                                </div>
                                <div class="bottom-line">
                                    <div class="row align-items-center">
                                        <div class="col-xl-7 col-md-6">
                                            <div class="row result-block align-items-center">
                                                <div class="col-auto result-block__title">Общая сумма заказа:</div>
                                                <div class="col-auto result-block__value">{{ $total }} ₽</div>
                                            </div>
                                        </div>
                                        <div class="col-xl-5 col-md-6">
                                            <div class="form-button form-button--right">
                                                <button class="btn btn-main button" type="submit">Оплатить</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection