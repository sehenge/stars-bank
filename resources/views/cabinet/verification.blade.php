@extends('layouts.cabinet')

@section('content')
    <main class="content page-content">
        <div class="container">
            <section class="section-main">
                @include('includes.cabinet.breadcrumbs')
                <div class="row">
                    @include('includes.cabinet.sidebar')
                    <div class="col-lg-9 col-md-8">
                        @if (Session::has('success'))
                            <div class="alert alert-success">
                                <i class="fas fa-check-circle"></i> {{ Session::get('success') }}
                            </div>
                        @endif
                        @if(count($errors) > 0)
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <ul class="p-0 m-0" style="list-style: none;">
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="page-block">
                            <div class="top-line">
                                <h2 class="page-block__title">Верификация мобильного телефона</h2>
                            </div>

                            @if(!$user->phone_verified_at)
                                @if(!$user->nexmo_request_id)
                                    <div class="block-form">
                                        <form class="form-login form-login--reg" action="{{ route('nexmo.sendcode') }}"
                                              method="POST">
                                            @csrf
                                            <div class="form-block">
                                                <div class="form-item">
                                                    <label class="form-label" for="nexmomobile">Мобильный телефон для
                                                        верификации:</label>
                                                    <input class="form-input" value="{{ $user->phone }}" type="text"
                                                           name="nexmomobile" id="nexmomobile"
                                                           pattern="\d*"
                                                           minlength="11" maxlength="14" disabled>
                                                </div>
                                                <div class="form-bottom row justify-content-between align-items-center">
                                                    <div class="col-auto form-bottom__item">
                                                        <div class="form-button">
                                                            <button class="btn btn-main button" type="submit">Отправить
                                                                код
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                @else
                                    <div class="block-form">
                                        <form class="form-login form-login--reg" action="{{ route('nexmo.verify') }}"
                                              method="POST">
                                            @csrf
                                            <div class="form-item">
                                                <label class="form-label" for="nexmocode">Ваш код верификации из
                                                    SMS:</label>
                                                <input class="form-input" type="text" name="nexmocode" id="nexmocode"
                                                       pattern="\d*"
                                                       minlength="4" maxlength="4">
                                            </div>
                                            <div class="form-bottom row justify-content-between align-items-center">
                                                <div class="col-auto form-bottom__item">
                                                    <div class="form-button">
                                                        <button class="btn btn-main button" type="submit">Активировать
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                @endif
                            @else
                                <div class="alert alert-success">
                                    <i class="fas fa-check-circle"></i> Телефон: {{ $user->phone }} успешно
                                    верифицирован!
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection
