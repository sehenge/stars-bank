@extends('layouts.cabinet')

@section('content')
    <main class="content page-content">
        <div class="container">
            <section class="section-main">
                @if(count($errors) > 0 )
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul class="p-0 m-0" style="list-style: none;">
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="form-order" action="{{ route('order.addtocart') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-7">
                            <div class="content-page-top">
                                <nav class="page-navigation">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{ route('index') }}">Главная</a></li>
                                        <li class="breadcrumb-item active">Оформление заказа</li>
                                    </ol>
                                </nav>
                                <h1 class="page-title">Оформление заказа</h1>
                                <p class="section-descr">Внимание! Для того чтобы получить максимально качественный результат необходимо заполнить все поля! Если у вас возникнут вопросы - свяжитесь с нами</p>
                            </div>
                            <div class="form-block">
                                <div class="form-item">
                                    <label class="form-label" for="name">Какое заведение рекламируем?</label>
                                    <input class="form-input" type="text" name="name" id="name">
                                </div>
                                <div class="form-item">
                                    <label class="form-label" for="address">Укажите адрес заведения:</label>
                                    <input class="form-input" type="text" name="address" id="address">
                                </div>
                                <div class="form-item">
                                    <label class="form-label" for="text">Как рекламируем? (Акция, УТП или рекламный текст)</label>
                                    <textarea class="form-input form-textarea js-textarea" name="text" id="text"></textarea>
                                </div>
                                <div class="form-item">
                                    <div class="form-label">Дополнительные опции:</div>
                                    <div class="row checkbox-group">
                                        <div class="col-lg-4 checkbox-group__item">
                                            <div class="checkbox-item">
                                                <input type="checkbox" id="chk1" name="additional_options[]" value="Представиться">
                                                <label for="chk1"><span class="check-icon"></span><span class="check-text">Представиться</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 checkbox-group__item">
                                            <div class="checkbox-item">
                                                <input type="checkbox" id="chk2" name="additional_options[]" value="Добавить музыку">
                                                <label for="chk2"><span class="check-icon"></span><span class="check-text">Добавить музыку</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 checkbox-group__item">
                                            <div class="checkbox-item">
                                                <input type="checkbox" id="chk3" name="additional_options[]" value="Представиться">
                                                <label for="chk3"><span class="check-icon"></span><span class="check-text">Представиться</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 checkbox-group__item">
                                            <div class="checkbox-item">                                            
                                                <input type="checkbox" id="chk4" name="additional_options[]" value="Пропеть название">
                                                <label for="chk4"><span class="check-icon"></span><span class="check-text">Пропеть название</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 checkbox-group__item">
                                            <div class="checkbox-item">
                                                <input type="checkbox" id="chk5" name="additional_options[]" value="Снять видео">
                                                <label for="chk5"><span class="check-icon"></span><span class="check-text">Снять видео</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 checkbox-group__item">
                                            <div class="checkbox-item">
                                                <input type="checkbox" id="chk6" name="additional_options[]" value="Пропеть название">
                                                <label for="chk6"><span class="check-icon"></span><span class="check-text">Пропеть название</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            @include('includes.order.info')
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </main>
@endsection
