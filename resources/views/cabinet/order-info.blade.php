@extends('layouts.cabinet')

@section('content')
    <main class="content page-content">
        <div class="container">
            <section class="section-main">
                <div class="row">
                    <div class="col-md-7">
                        @include('includes.cabinet.breadcrumbs')
                        <div class="info-order">
                            <div class="info-order__item">
                                <div class="info-order-card">
                                    <div class="info-order-card__title">Дата заказа:</div>
                                    <div class="info-order-card__value">{{ $order->created_at }}</div>
                                </div>
                            </div>
                            <div class="info-order__item">
                                <div class="info-order-card">
                                    <div class="info-order-card__title">Статус заказа:</div>
                                    <div class="info-order-card__value">{{ $order->status }}</div>
                                </div>
                            </div>
                            <div class="info-order__item">
                                <div class="info-order-card">
                                    <div class="info-order-card__title">Рекламируемое заведение:</div>
                                    <div class="info-order-card__value">{{ $order->name }}</div>
                                </div>
                            </div>
                            <div class="info-order__item">
                                <div class="info-order-card">
                                    <div class="info-order-card__title">Адрес заведения:</div>
                                    <div class="info-order-card__value">{{ $order->address }}</div>
                                </div>
                            </div>
                            <div class="info-order__item">
                                <div class="info-order-card">
                                    <div class="info-order-card__title">Рекламный текст:</div>
                                    <div class="info-order-card__value">{{ $order->advert_text }}</div>
                                </div>
                            </div>
                            <div class="info-order__item">
                                <div class="info-order-card">
                                    <div class="info-order-card__title">Дополнительные опции:</div>
                                    
                                    @foreach($order->additional_options as $option)
                                        <div class="info-order-card__value">{{ $option }}</div>
                                    @endforeach  
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="annotation-block">
                            <div class="annotation-block__top">
                                <h2 class="annotation-block__title">ИНФОРМАЦИЯ О ЗАКАЗЕ</h2>
                            </div>
                            <div class="annotation-block__middle">
                                <div class="result-order">
                                    <div class="result-order__avatar"><img class="img-fluid" src="/assets/img/images/avatar-star.png" alt=""></div>
                                    <div class="result-order__text">Всем привет, я Полина Гагарина и я хочу сказать, что в Саратове на улице Неделина дом 15 самый вкусный и ароматный капуччино, который я когда-либо пила в своей жизни! Всем советую!</div>
                                </div>
                            </div>
                            <div class="annotation-block__bottom">
                                <div class="block-descr">
                                    <div class="card-descr">
                                        <div class="card-descr__item">
                                            <svg class="svg-icon icon-clock ">
                                                <use xlink:href="./assets/img/spriteSvg.svg#sprite-clock"></use>
                                            </svg>Срок выполнения: <b>5 дней</b>
                                        </div>
                                        <div class="card-descr__item">
                                            <svg class="svg-icon icon-label ">
                                                <use xlink:href="./assets/img/spriteSvg.svg#sprite-label"></use>
                                            </svg>Стоимость: <b class="price">{{ $order->price }} ₽</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection
