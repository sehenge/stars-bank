@extends('layouts.cabinet')

@section('content')
    <main class="content page-content">
        <div class="container">
            <section class="section-main">
                @include('includes.cabinet.breadcrumbs')
                <div class="row">
                    @include('includes.cabinet.sidebar')
                    <div class="col-lg-9 col-md-8">
                        <div class="page-block">
                            <div class="top-line">
                                <h2 class="page-block__title">Мои заказы</h2>
                            </div>
                            <div class="block-table">
                                <div class="cart-list">
                                    @foreach($orders as $order)
                                        <a class="cart-list__item" href="{{ route('order.details', ['id' => $order->id]) }}">
                                            <div class="form-row align-items-center">
                                                <div class="col-md-2 col-sm-2">
                                                    <div class="cart-date">{{ $order->created_at }}</div>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="cart-order-number">{{ $order->name }}</div>
                                                </div>
                                                <div class="col-md-2 col-sm-2">
                                                    <div class="cart-price">{{ $order->price }} ₽</div>
                                                </div>
                                                <div class="col-md-2 col-sm-2">
                                                    <div class="cart-status">{{ $order->status }}</div>
                                                </div>
                                            </div>
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection
