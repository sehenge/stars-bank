@extends('layouts.master')

@section('content')
    <main class="content form-content">
        <div class="container">
            <div class="center-block center-block--form">
                <h1 class="center-block__title">Восстановление пароля</h1>
                <form class="form-login">
                    <div class="form-item">
                        <label class="form-label" for="email">Ваш email:</label>
                        <input class="form-input" type="email" name="email" id="email" placeholder="Email">
                    </div>
                    <div class="form-button center">
                        <button class="btn btn-main button" type="submit">Восстановить</button>
                    </div>
                </form>
            </div>
        </div>
    </main>
@endsection
