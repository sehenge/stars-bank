@extends('layouts.master')

@section('content')
    <main class="content page-content">
        <div class="container">
            <section class="section-main">
                <div class="content-page-top">
                    <nav class="page-navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('index') }}">Главная</a></li>
                            <li class="breadcrumb-item active">Контакты</li>
                        </ol>
                    </nav>
                    <h1 class="page-title">Контакты</h1>
                </div>
                <div class="support-block">
                    <div class="row support-block__list">
                        <div class="col-lg-3 col-md-6 support-block__item">
                            <div class="support-card">
                                <div class="support-card__title">Горячая линия:</div>
                                <ul class="support-card__list">
                                    <li><a href="tel:89999999999">
                                            <svg class="svg-icon icon-smartphone ">
                                                <use xlink:href="/assets/img/spriteSvg.svg#sprite-smartphone"></use>
                                            </svg>8 999 999 99 99</a></li>
                                    <li><a href="mailto:info@mail.ru">
                                            <svg class="svg-icon icon-email ">
                                                <use xlink:href="/assets/img/spriteSvg.svg#sprite-email"></use>
                                            </svg>info@mail.ru</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 support-block__item">
                            <div class="support-card">
                                <div class="support-card__title">По вопросам сотрудничества:</div>
                                <ul class="support-card__list">
                                    <li><a href="tel:89999999999">
                                            <svg class="svg-icon icon-smartphone ">
                                                <use xlink:href="/assets/img/spriteSvg.svg#sprite-smartphone"></use>
                                            </svg>8 999 999 99 99</a></li>
                                    <li><a href="mailto:info@mail.ru">
                                            <svg class="svg-icon icon-email ">
                                                <use xlink:href="/assets/img/spriteSvg.svg#sprite-email"></use>
                                            </svg>info@mail.ru</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 support-block__item">
                            <div class="support-card">
                                <div class="support-card__title">Горячая линия:</div>
                                <ul class="support-card__list">
                                    <li><a href="tel:89999999999">
                                            <svg class="svg-icon icon-smartphone ">
                                                <use xlink:href="/assets/img/spriteSvg.svg#sprite-smartphone"></use>
                                            </svg>8 999 999 99 99</a></li>
                                    <li><a href="mailto:info@mail.ru">
                                            <svg class="svg-icon icon-email ">
                                                <use xlink:href="/assets/img/spriteSvg.svg#sprite-email"></use>
                                            </svg>info@mail.ru</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 support-block__item">
                            <div class="support-card">
                                <div class="support-card__title">По вопросам сотрудничества:</div>
                                <ul class="support-card__list">
                                    <li><a href="tel:89999999999">
                                            <svg class="svg-icon icon-smartphone ">
                                                <use xlink:href="/assets/img/spriteSvg.svg#sprite-smartphone"></use>
                                            </svg>8 999 999 99 99</a></li>
                                    <li><a href="mailto:info@mail.ru">
                                            <svg class="svg-icon icon-email ">
                                                <use xlink:href="/assets/img/spriteSvg.svg#sprite-email"></use>
                                            </svg>info@mail.ru</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="social-buttons">
                        <ul>
                            <li><a href="#" target="_blank">
                                    <svg class="svg-icon icon-vk ">
                                        <use xlink:href="/assets/img/spriteSvg.svg#sprite-vk"></use>
                                    </svg></a></li>
                            <li><a href="#" target="_blank">
                                    <svg class="svg-icon icon-fb ">
                                        <use xlink:href="/assets/img/spriteSvg.svg#sprite-fb"></use>
                                    </svg></a></li>
                            <li><a href="#" target="_blank">
                                    <svg class="svg-icon icon-instagram ">
                                        <use xlink:href="/assets/img/spriteSvg.svg#sprite-instagram"></use>
                                    </svg></a></li>
                        </ul>
                    </div>
                </div>
                <div class="separate-line separate-line--white"></div>
                <div class="form-contact-block">
                    <div class="content-page-top">
                        <h1 class="page-title">Обратная связь</h1>
                        <p class="section-descr">Если у вас остались вопросы - напишите нам и менеджер свяжется с вами в течение 15 минут</p>
                    </div>
                    <form class="form-contact">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="form-item">
                                    <input class="form-input" type="tel" name="phone" placeholder="Номер телефона">
                                </div>
                                <div class="form-item">
                                    <input class="form-input" type="email" name="email" placeholder="Ваш Email">
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="form-item">
                                    <textarea class="form-input form-textarea" name="text" placeholder="Текст вопроса"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-button">
                            <button class="btn btn--long btn-main button" type="submit">Отправить</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </main>
@endsection
