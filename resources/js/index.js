// Main js file
import $ from "jquery"
window.jQuery = $;

import mask from 'jquery.maskedinput/src/jquery.maskedinput'
import ionRangeSlider from 'ion-rangeslider'
//import niceScroll from 'jquery.nicescroll'
import selectize from 'selectize'
require("@fancyapps/fancybox");
import autosize from 'autosize'
import svg4everybody from 'svg4everybody'





$(document).ready(function () {

	$('body').on('click','.js-card .js-delete-product',function () {
		$(this).closest('.js-cart-item').remove()
	})

	$("input[type=tel]").mask("+9 999-999-99-99")

	$('.js-select').selectize()

	svg4everybody()

	$('[data-fancybox]').fancybox({
		closeExisting: true,
		clickOutside: "close",
	});

	$.fancybox.defaults.backFocus = false;


	var screenWidth = $(window).width()

	function setting() {
		if(screenWidth < 1024) {
			var list = $('.js-setting-dropdown'),
				btn = $('.js-setting-button');

			$(document).mouseup(function (e) {
				if (!$('.user-setting').is(e.target)
					&& $('.user-setting').has(e.target).length === 0) {
					list.removeClass('open');
					btn.removeClass('active');
				}
			});


			$('.js-setting-button').click(function () {
				$(this).toggleClass('active')
				$('.js-setting-dropdown').toggleClass('open')
			})
		}
	}

	setting()

	// if(screenWidth > 1199) {
	// 	$('.check-list').niceScroll({
	// 		scrollspeed :  150, // скорость прокрутки
	// 		mousescrollstep :  20,
	// 		hwacceleration: true,
	// 		bouncescroll :  true,
	// 		zindex :  "50",
	// 		cursorborderradius :  "6px",
	// 		cursorborder: "none",
	// 		horizrailenabled: false,
	// 		cursorcolor: '#FFFFFF',
	// 		cursorwidth: '10px',
	// 		disablemutationobserver: true,
	// 		autohidemode: false,
	// 	})
	// }

	$('body').on('click', '.js-m-search-btn', function () {
		$(this).toggleClass('active')
		$('.js-search-form').toggleClass('open')
	})


	function range() {
		var $range = $(".js-range-slider"),
			$inputFrom = $(".js-input-from"),
			$inputTo = $(".js-input-to"),
			instance,
			min = $(".js-range-slider").data('min'),
			max = $(".js-range-slider").data('max'),
			from = 0,
			to = 0;

		$range.ionRangeSlider({
			skin: "round",
			onStart: updateInputs,
			onChange: updateInputs
		});
		instance = $range.data("ionRangeSlider");

		function updateInputs (data) {
			from = data.from;
			to = data.to;

			$inputFrom.prop("value", from);
			$inputTo.prop("value", to);
		}

		$inputFrom.on("input", function () {
			var val = $(this).prop("value");

			// validate
			if (val < min) {
				val = min;
			} else if (val > to) {
				val = to;
			}

			instance.update({
				from: val
			});
		});

		$inputTo.on("input", function () {
			var val = $(this).prop("value");

			// validate
			if (val < from) {
				val = from;
			} else if (val > max) {
				val = max;
			}

			instance.update({
				to: val
			});
		});
	}

	range()

	$(".js-number-field").keypress(function (e) {
		if (e.which != 8 && e.which != 0 && e.which != 46 && e.which != 44 && (e.which < 48 || e.which > 57)) {
			return false;
		}
	});

	function audioUpload() {
		if (!$('.js-file').length) {
			return false;
		}
		$('.js-file').change(function () {
			const input = $(this)[0];

			if (input.files && input.files[0]) {
				if (input.files[0].type.match('audio.*')) {
					const reader = new FileReader();
					reader.onload = function(e) {
						$('#file-audio').text(input.files[0].name)
					}
					reader.readAsDataURL(input.files[0]);
					$('#file-audio').show();
				} else {
					alert("Это не аудио файл.");
				}
			} else {
				console.log('Error!');
			}
		})
	}

	audioUpload()

	function avatarUpload() {

		if (!$('#img').length) {
			return false;
		}

		const uploadAvatar = $('#img').change(function() {
			const input = $(this)[0];
			if (input.files && input.files[0]) {
				if (input.files[0].type.match('image.*')) {
					const reader = new FileReader();
					reader.onload = function(e) {
						$('#img-preview').attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
					$('#reset-img-preview').show();
				} else {
					alert("Этот файл не изображение.");
				}
			} else {
				console.log('Error!');
			}
		});

		$('#btn-upload').click(function(e) {
			uploadAvatar
		})

		$('#reset-img-preview').click(function(e) {
			e.preventDefault();
			$('#img').val('');
			$('#img-preview').attr('src', '/assets/img/images/users.svg');
			$('#reset-img-preview').hide();
		});

	}

	avatarUpload()

	autosize($('.js-textarea'));

})



//button
var button = document.querySelectorAll('.button');
for (var i = 0; i < button.length; i++) {
	button[i].onmousedown = function(e) {

		var x = (e.offsetX == undefined) ? e.layerX : e.offsetX;
		var y = (e.offsetY == undefined) ? e.layerY : e.offsetY;
		var effect = document.createElement('div');
		effect.className = 'effect';
		effect.style.top = y + 'px';
		effect.style.left = x + 'px';
		e.target.appendChild(effect);
		setTimeout(function() {
			e.target.removeChild(effect);
		}, 1100);
	}
}
