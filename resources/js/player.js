import Plyr from 'plyr'

jQuery(function ($) {
	let index = 0
	$('body').on('click','.js-card .js-delete-product',function () {
		$(this).closest('.js-cart-item').remove()
	})

	player()
	function player() {
		if (!$('.js-player').length) {
			return false;
		}
		const supportsAudio = !!document.createElement('audio').canPlayType;

		if (supportsAudio) {
			// initialize plyr
			const player = new Plyr('#player', {
				controls: [
					//'play',
					'progress',
				]
			});

			const player2 = new Plyr('#player2', {
				controls: [
					//'play',
					'progress',
				]
			})
			// initialize playlist and controls
			let playing = false
			let playing2 = false
			let extension = ''
			const tracks = []
			const items = $('.js-card').each(function() {
				let track = $(this).data('track')
				tracks.push(track)
			})
			const trackCount = tracks.length
			$('.js-card').each(function(index) {
				$(this).attr('data-index', index)
			})
			const audio = $('#player').on('play', function () {
				playing = true
			}).on('pause', function () {
				playing = false
			}).get(0)

			const audio2 = $('#player2').on('play', function () {
				playing2 = true
			}).on('pause', function () {
				playing2 = false
			}).get(0)

			const btnPlay2 = $('#btnPlay2').on('click', function () {
				if($('#player').length) {
					audio.pause()
					$('#btnPlay').removeClass('pause')
					$('.js-card-wrap.active').find('.js-play-track').removeClass('pause')
				}

				if (playing2) {
					audio2.pause()
					$(this).removeClass('pause')
				} else {
					audio2.play()
					$(this).addClass('pause')
				}
			})

			const btnPlay = $('#btnPlay').on('click', function () {
				if($('#player2').length) {
					audio2.pause()
					$('#btnPlay2').removeClass('pause')
				}

				if (playing) {
					audio.pause()
					$('.js-card-wrap.active').find('.js-play-track').removeClass('pause')
					$(this).removeClass('pause')
				} else {
					audio.play()
					$('.js-card-wrap.active').find('.js-play-track').addClass('pause')
					$(this).addClass('pause')
				}
			})
			const btnPrev = $('#btnPrev').on('click', function () {
				$('.js-card-wrap').removeClass('active')
				$('.js-card-wrap').find('.play-btn').removeClass('pause')
				if ((index - 1) > -1) {
					index--
					loadTrack(index)
					$('.js-card[data-index=' + index + ']').closest('.js-card-wrap').addClass('active')
					//audio.play();
					if (playing) {
						$('.js-card[data-index=' + index + ']').find('.play-btn').addClass('pause')
						audio.play()
					}
				} else {
					index = trackCount - 1;
					loadTrack(index)
					$('.js-card[data-index=' + index + ']').closest('.js-card-wrap').addClass('active')
					if (playing) {
						$('.js-card[data-index=' + index + ']').find('.play-btn').addClass('pause')
						audio.play()
					}
				}
			})
			const btnNext = $('#btnNext').on('click', function () {
				$('.js-card-wrap').removeClass('active')
				$('.js-card-wrap').find('.play-btn').removeClass('pause')
				if ((index + 1) < trackCount) {
					index++
					loadTrack(index)
					$('.js-card[data-index=' + index + ']').closest('.js-card-wrap').addClass('active')
					if (playing) {
						$('.js-card[data-index=' + index + ']').find('.play-btn').addClass('pause')
						audio.play()
					}
				} else {
					index = 0
					$('.js-card[data-index=' + index + ']').closest('.js-card-wrap').addClass('active')
					loadTrack(index)
					if (playing) {
						$('.js-card[data-index=' + index + ']').find('.play-btn').addClass('pause')
						audio.play()
					}
				}
			})
			$('.js-play-track').on('click',function () {
				if($('#player2').length) {
					audio2.pause()
					$('#btnPlay2').removeClass('pause')
				}

				if(!$(this).hasClass('pause')) {
					$('.player-block').addClass('open')
					$('.js-card-wrap').removeClass('active')
					$(this).closest('.js-card-wrap').addClass('active')
					index = $(this).closest('.js-card').data('index')
					loadTrack(index)

					$('.js-play-track').removeClass('pause')
					audio.play()
					$(this).addClass('pause')
					btnPlay.addClass('pause')
				} else {
					audio.pause()
					$(this).removeClass('pause')
					btnPlay.removeClass('pause')
					$(this).closest('.js-card-wrap').removeClass('active')
					$('.player-block').removeClass('open')
				}
			})

			let loadTrack = function (id) {
				index = id;
				let card = $('.js-card[data-index=' + index + ']')
				let avatar = card.data('avatar')
				let title = card.data('title')
				let author = card.data('author')
				$('#track-avatar img').attr('src',avatar)
				$('#track-name').text(title)
				$('#track-author').text(author)
				audio.src = tracks[id]

			}
			let playTrack = function (id) {
				loadTrack(id)
				audio.play()
			}

			$('#close-btn').on('click',function() {
				$('.js-card-wrap').removeClass('active')
				$('.player-block').removeClass('open')
				$('.js-card[data-index=' + index + ']').find('.play-btn').removeClass('pause')
				audio.pause()
			})

			extension = audio.canPlayType('audio/mpeg') ? '.mp3' : audio.canPlayType('audio/ogg') ? '.ogg' : ''
			loadTrack(index)
		} else {
			// no audio support
			var noSupport = $('#player').text()
			$('.player__container').append('<p class="no-support">' + noSupport + '</p>')
		}
	}


});
