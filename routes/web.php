<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\StarController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainController::class, 'index'])->name('index');

/*Route::prefix('cabinet')->group(function () {
    Route::get('login', 'AuthController@index')->name('cabinet.login');
    Route::get('logout', 'AuthController@logout')->name('cabinet.logout');
    Route::post('login', 'AuthController@login')->name('cabinet.loginpost');
    Route::post('registration', 'AuthController@register')->name('cabinet.register');
    Route::view('registration', 'cabinet.registration')->name('cabinet.registration');
});*/

Route::middleware(['verified'])->group(function() {
    Route::view('restore', 'cabinet.restore')->name('cabinet.restore');
    Route::view('cart', 'cabinet.cart')->name('cabinet.cart');

    Route::get('profile', [ProfileController::class, 'index'])->name('cabinet.profile');
    Route::post('profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::get('verification', [ProfileController::class, 'verificationPage'])->name('cabinet.verification');

    Route::get('history', [OrderController::class, 'showHistory'])->name('cabinet.history');


    //TODO: move to phone.verified
    Route::post('order/place', [OrderController::class, 'place'])->name('order.place');
    Route::post('order/details/{id}', [OrderController::class, 'show'])->name('order.details');
    
    Route::post('orders', [OrderController::class, 'index'])->name('orders');

    Route::post('addtocart', [StarController::class, 'addToCart'])->name('order.addtocart');
    Route::delete('removefromcart', [StarController::class, 'removeFromCart'])->name('order.removefromcart');

    Route::post('nexmo/verify', [AuthController::class, 'phoneVerify'])->name('nexmo.verify');
    Route::post('nexmo/sendcode', [AuthController::class, 'phoneVerificationSendCode'])->name('nexmo.sendcode');

    Route::group(['prefix' => 'admin'], function () {
        Voyager::routes();
    });
});
    
Route::middleware(['phone.verified'])->group(function() {
    Route::get('order/{id}', [OrderController::class, 'create'])->name('order');
});

Route::get('card-product/{id}', [StarController::class, 'show'])->name('card.product');
Route::view('/contacts', 'contacts')->name('contacts');
Route::view('/policy', 'policy')->name('policy');

Auth::routes(['verify' => true]);

Route::get('login', 'AuthController@index')->name('login');
Route::post('login', 'AuthController@login')->name('loginpost');
// Route::get('/posts', 'PostController@index');

